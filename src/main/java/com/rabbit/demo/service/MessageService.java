package com.rabbit.demo.service;

import com.rabbit.demo.model.Message;
import com.rabbit.demo.repository.IMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@Service
public class MessageService {
    @Autowired
    private IMessageRepository iMessageRepository;

    public Message saveMessage(Message message) {
        if (message.getId() == null) {
            return iMessageRepository.save(message);
        }
        return null;
    }

    public Page<Message> getAllMessage(Integer page, Integer size, Boolean enablePagination) {
        return iMessageRepository.findAll(enablePagination ? PageRequest.of(page, size) : Pageable.unpaged());
    }

    public Optional<Message> findById(Long id) {
        return iMessageRepository.findById(id);
    }

    public void deleteMessage(Long id) {
        iMessageRepository.deleteById(id);
    }



    public Message editMessage(Message message) {
        if (message.getId() != null && iMessageRepository.existsById(message.getId())) {
            return iMessageRepository.save(message);
        }
        return null;
    }

    public boolean existById(Long id) {
        return iMessageRepository.existsById(id);
    }
}

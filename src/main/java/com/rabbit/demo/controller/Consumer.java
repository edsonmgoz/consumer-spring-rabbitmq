package com.rabbit.demo.controller;

import com.rabbit.demo.model.Message;
import com.rabbit.demo.service.MessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Component
@Slf4j
public class Consumer {
    @Autowired
    private MessageService messageService;
    @RabbitListener(queues = "queue.A")
    private void receive(Message message) throws InterruptedException {
        Thread.sleep(5*1000);
        log.info("Message received form QUEUE A->{}", message);
        message.setFechaRecepcion(Timestamp.valueOf(LocalDateTime.now()));
        message.setState("recibido");
        messageService.editMessage(message);
    }

    @RabbitListener(queues = "queue.B")
    private void receiveFromB(Message message) throws InterruptedException {
        Thread.sleep(5*1000);
        log.info("Message received form QUEUE B->{}", message);
        message.setFechaRecepcion(Timestamp.valueOf(LocalDateTime.now()));
        message.setState("recibido");
        messageService.editMessage(message);

    }
}
